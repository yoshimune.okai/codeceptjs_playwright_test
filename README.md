# codeceptJSを用いたe2eテスト
### 利用ツールと主な機能
- codeceptJS
- playwright
- allure

### 主な機能
- スクリーンショットの撮影
- ビジュアルテスト(画像差分)
- ランタイム計測
- レポーティング機能

### ビジュアルテストについて
一つ前に取得した(同じ名前の)スクリーンショットと画像比較し，差分をハイライト表示
> 設定ファイルはcodecept.conf.js
> 差分画像は./tests/screenshots/diff
