Feature('search');

Scenario('search keyword', ({ I }) => {
	I.amOnPage("https://qiita.com/");
	I.seeInTitle("Qiita");
	I.see("How developers code is here.");
	I.fillField("キーワードを入力","codeceptJS")
	I.pressKey("Enter")
	// I.click("マイルストーン");
	I.wait(1);
	I.saveScreenshot("./screenshots/Qiita_search.png",true);
	// I.seeVisualDiff("Qiita_top.png",{tolerance:2, prepareBaseImage: false}); //ビジュアルテスト
});


Scenario('move page', ({ I }) => {
	I.see("CodeceptJSでオートコンプリートする");
	I.click("CodeceptJSでオートコンプリートする");
	I.see("ちょっとだけ補足","h2");
	I.saveScreenshot("./screenshots/Qiita_page1.png",true);
	// I.seeVisualDiff("Qiita_top.png",{tolerance:2, prepareBaseImage: false}); //ビジュアルテスト
});
